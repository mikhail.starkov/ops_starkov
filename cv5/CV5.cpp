/*
 * Mikhail Starkov
 * Program pracuje s threads a vypisuje Info o vlákně
 */
#include "mbed.h"
#include <inttypes.h>
#include "stm32746g_discovery_lcd.h"
 
#if !defined(MBED_THREAD_STATS_ENABLED)
#error "Thread statistics not enabled"
#endif
 
#define MAX_THREAD_STATS    0x8
#define BLINKY_THREAD_STACK 224
#define IDLE_THREAD_STACK 384
#define STOP_FLAG 0xEF
#define WAIT_TIME_MS 500

DigitalOut led(LED1);

void display(char input[], int line, int time);
void display_thread1();
void display_thread2();
void display_thread3();
void display_thread4();

int main()
{
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);

    BSP_LCD_Clear(LCD_COLOR_ORANGE);
    BSP_LCD_SetFont(&LCD_DEFAULT_FONT);
    BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
    
    Thread *thread1 = new Thread(osPriorityNormal, OS_STACK_SIZE, nullptr, "Thread1");
    Thread *thread2 = new Thread(osPriorityNormal, OS_STACK_SIZE, nullptr, "Thread2");
    Thread *thread3 = new Thread(osPriorityNormal, OS_STACK_SIZE, nullptr, "Thread3");
    Thread *thread4 = new Thread(osPriorityNormal, OS_STACK_SIZE, nullptr, "Thread4");
  
    thread1->start(display_thread1);
    thread2->start(display_thread2);
    thread3->start(display_thread3);
    thread4->start(display_thread4);   
     
    while (true){
        }
    // Sleep helps other created threads to run
    thread_sleep_for(10);
    mbed_stats_thread_t *stats = new mbed_stats_thread_t[MAX_THREAD_STATS];
    int count = mbed_stats_thread_get_each(stats, MAX_THREAD_STATS);

    for (int i = 0; i < count; i++) {
        printf("ID: 0x%" PRIx32 "\n", stats[i].id);
        printf("Name: %s \n", stats[i].name);
        printf("State: %" PRId32 "\n", stats[i].state);
        printf("Priority: %" PRId32 "\n", stats[i].priority);
        printf("Stack Size: %" PRId32 "\n", stats[i].stack_size);
        printf("Stack Space: %" PRId32 "\n", stats[i].stack_space);
        printf("\n");
    }
   
    thread1->terminate();
    thread2->terminate();   
    thread3->terminate();  
    thread4->terminate();    
    return 0;

}

void display(char input[], int line, int time)
{
    while(1){
        BSP_LCD_DisplayStringAtLine(line, (uint8_t *)input);   
        ThisThread::sleep_for(time);
        BSP_LCD_ClearStringLine(line);
        ThisThread::sleep_for(time);
    }
}


void display_thread1()
{
        while(1){
        display("My name is Mikhail Starkov", 1, 600);
    }

}

void display_thread2()
{
    while(1){
        display("What is your name?", 3, 1000);
    }
}

void display_thread3()
{
        while(1){
        display("AHOJ!", 5, 100);
    }
}

void display_thread4(){
     while(1){
        BSP_LCD_DisplayOn();
        ThisThread::sleep_for(8000);
        BSP_LCD_DisplayOff();
        ThisThread::sleep_for(1000);
    }   
}
