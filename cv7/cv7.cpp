/*
 * Mikhail Starkov
 * Deska po startu napíše na displeji, proč byla naposledy restartována
 */
#include "mbed.h"
#include "stm32746g_discovery_lcd.h"
#include <cstdint>
#include "ResetReason.h"
#include <string>
#include <cstdio>

static BufferedSerial pc(USBTX, USBRX);
Thread reading;

std::string reset_reason_to_string(const reset_reason_t reason)
{
    switch (reason) {
        case RESET_REASON_POWER_ON:
            return "Power On";
        case RESET_REASON_PIN_RESET:
            return "Button";
        case RESET_REASON_SOFTWARE:
            return "Software Reset";
        case RESET_REASON_WATCHDOG:
            return "Watchdog";
        default:
            return "Other Reason";
    }
}

void read_serial(){
    char c;
    char reset = 'r';
    while (true) {
        pc.read(&c, sizeof(string));
        if(c == reset){
            NVIC_SystemReset();
        }
    }
}

int main() {
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
    BSP_LCD_Clear(LCD_COLOR_WHITE);
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    const reset_reason_t reason = ResetReason::get();
    
    BSP_LCD_DisplayStringAt(0, 120, (uint8_t *)reset_reason_to_string(reason).c_str(), CENTER_MODE);
    
    Watchdog &watchdog = Watchdog::get_instance();
    watchdog.start(5000);
    
    reading.start(read_serial);
    
    while(true) {

    }

}
