/*
 * Mikhail Starkov
 * Program výpisuje text do sériového portu
 */
#include "mbed.h"
#include <cstdint>

static BufferedSerial pc(USBTX, USBRX);
DigitalIn btn(BUTTON1);

void printToSerialPort(){
    btn.mode(PullDown);
    char messageGreating[] = "Tuk tuk, je tu nekdo?\n";
    char messageType[] = "Ahoj!\nStisknutim tlacitka prectete znovu\n";
    uint8_t i = 0, size = sizeof(messageType), lastState = btn.read(),
    currentState;
    pc.write(messageGreating,sizeof(messageGreating));
    while(true){
        currentState = btn.read();
        if (currentState != lastState) {
            lastState = currentState;
            if(currentState){
              pc.write(&messageType[i], sizeof(char));
              i = ++i % size;
            }
        }
    }
}
    Thread btnThread;

int main()
{
    btnThread.start(printToSerialPort);
    btnThread.join();
}