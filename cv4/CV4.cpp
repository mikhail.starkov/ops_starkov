/*
 * Mikhail Starkov
 * Program po stisknutí tlačítka nakreslí obdélník
 */

#include "mbed.h"
#include "stm32746g_discovery_lcd.h"

InterruptIn sw(BUTTON1);
EventQueue queue(32 * EVENTS_EVENT_SIZE);
Thread thread;

void rise_handler(void)
{
    queue.call(printf, "rise_handler in context %p\n", ThisThread::get_id()); 
    queue.call(BSP_LCD_SetTextColor,LCD_COLOR_YELLOW);
    queue.call(BSP_LCD_FillRect,100, 100, 200,140); //void BSP_LCD_FillRect(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height)
}  

void fall_handler(void)
{
    queue.call(printf,"fall_handler in context %p\n", ThisThread::get_id()); 
    queue.call(BSP_LCD_SetTextColor,LCD_COLOR_BLACK);
    queue.call(BSP_LCD_FillRect,100, 100, 200,140); //void BSP_LCD_FillRect(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height)
}

int main()
{
    thread.start(callback(&queue, &EventQueue::dispatch_forever));
    printf("Starting in context %p\r\n", ThisThread::get_id());
    sw.rise(rise_handler);
    sw.fall(fall_handler);
    
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
    BSP_LCD_Clear(LCD_COLOR_BLACK);
    
    while (true){
            
            }
}