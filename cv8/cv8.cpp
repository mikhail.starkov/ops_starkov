/*
 * Mikhail Starkov
 * Vytvoření časovače
 */
#include "mbed.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include <cstdio>
#include <stdio.h>
#include <string>
#include "StrLib.h"

Thread thread1, thread2, thread3;

struct Time {
   int   time;
};
struct Time timer;

void drawButtons(){
    //Xpos,:  X position
    //Ypos,:  Y position
    //Width,: Rectangle width
    //Height,:    Rectangle height
    BSP_LCD_DrawRect(10, 10, 100, 250); //Rect left - plus
    BSP_LCD_DrawRect(365, 10, 100, 250);//Rect right  - minus
    BSP_LCD_DrawRect(170, 200, 100, 60);//Rect down  left - set
    //BSP_LCD_DrawRect(255, 200, 100, 60);//Rect down  right - start
    BSP_LCD_DisplayStringAt(50, 125, (uint8_t *)"+", LEFT_MODE);
    BSP_LCD_DisplayStringAt(410, 125, (uint8_t *)"-", LEFT_MODE);
    BSP_LCD_DisplayStringAt(180, 210, (uint8_t *)"START", LEFT_MODE);
    //BSP_LCD_DisplayStringAt(280, 210, (uint8_t *)"SET", LEFT_MODE);
    //BSP_LCD_DisplayStringAt(0, 120, (uint8_t *)"00:02:00", CENTER_MODE);
    
}

void drawTime(){
        //time = 120;
        int seconds = timer.time; 
        int minutes = 0;
        int hours = 0;
        while (seconds >=60){
                minutes++;
                seconds-=60;
        }
        while (minutes>=60){
            hours++;
            minutes-=60;
        } 
        char fulltime[20];
        BSP_LCD_Clear(LCD_COLOR_ORANGE);
        drawButtons();
        //seconds = to_string(seconds);
        float param1=seconds;
        float param2=minutes;
        float param3=hours;
        int intpart1;
        int intpart2;
        int intpart3;
        intpart1=floor( param1) ;
        intpart2=floor( param2) ;
        intpart3=floor( param3) ;
        char str1[20];
        char str2[20];
        char str3[20];
        sprintf(str1,"%02i",intpart1);
        sprintf(str2,"%02i",intpart2);
        sprintf(str3,"%02i",intpart3); //
        BSP_LCD_DisplayStringAt(-40, 120, (uint8_t *)str3,CENTER_MODE);
        BSP_LCD_DisplayStringAt(-10, 120, (uint8_t *)":",CENTER_MODE);
        BSP_LCD_DisplayStringAt(20, 120, (uint8_t *)str2,CENTER_MODE);
        BSP_LCD_DisplayStringAt(50, 120, (uint8_t *)":",CENTER_MODE);
        BSP_LCD_DisplayStringAt(80, 120, (uint8_t *)str1,CENTER_MODE);
}

void plusTime(void){
    timer.time+=5;
    drawTime();
}

void minusTime(void){
    if (timer.time>=5) {
        timer.time-=5;
    drawTime();
    }
}

void startTime(int cas){
    while (timer.time!=0) {
            ThisThread::sleep_for(cas);
            timer.time-=1;
            drawTime();
    }
    BSP_LCD_Clear(LCD_COLOR_RED);
}

void setButtons(uint16_t x,uint16_t y,uint16_t last_x,uint16_t last_y){
    if (x!=last_x && y!=last_y) {
    if(x>10 && x<110 && y>10 && y<260){plusTime();}
    if(x>365 && x<465 && y>10 && y<260){minusTime();}
    if(x>170 && x<270 && y>200 && y<260){startTime(1000);}
    }
}

void touch_screen_reader(){
    TS_StateTypeDef TS_State;
    uint16_t x, y;
    uint16_t last_x=0, last_y=0;
    uint8_t status;
    status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
    uint8_t idx;
    while(1) {
        BSP_TS_GetState(&TS_State);
        if (TS_State.touchDetected) {
            x = TS_State.touchX[0];            
            y = TS_State.touchY[0];    
            setButtons(x,y,last_x,last_y);   
            last_x=x;
            last_y=y;     
        }
    }
}

void display_thread1()
{
        while(1){
        drawTime();
    }

}

void display_thread2()
{
        while(1){
        touch_screen_reader();
    }
}

int main()
{
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
    BSP_LCD_SetFont(&Font24);
    BSP_LCD_Clear(LCD_COLOR_ORANGE);
    BSP_LCD_SetBackColor(LCD_COLOR_ORANGE);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    drawButtons();
    timer.time=120;
    drawTime();
    //thread1.start(display_thread1);
    thread2.start(display_thread2);
            while (true){    
            }
}
